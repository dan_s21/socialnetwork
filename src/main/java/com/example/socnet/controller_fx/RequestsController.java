package com.example.socnet.controller_fx;

import com.example.socnet.domain.model.FriendshipRequest;
import com.example.socnet.domain.util.Constants;
import com.example.socnet.resources.Resources;
import com.example.socnet.service.CurrentUser;
import com.example.socnet.service.RequestService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class RequestsController implements Initializable {

    @FXML public ListView<FriendshipRequest> lvFriendRequests;

    @FXML public Button btnGoBack;

    private final ObservableList<FriendshipRequest> friendshipRequests;
    private final RequestService requestService;

    public RequestsController() throws SQLException {
        this.friendshipRequests = FXCollections.observableArrayList();
        this.requestService = Resources.getInstance().getRequestService();
    }

    @FXML
    public void goBack(ActionEvent event) {
        NavController.navigate(Constants.UI.Scene.MAIN_FEED, event);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.lvFriendRequests.setItems(this.friendshipRequests);
        try {
            this.init();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void init() throws SQLException {
        final long userID = CurrentUser.getInstance().getUser().getId();
        this.friendshipRequests.addAll(this.requestService.getAllFor(userID));
    }
}
