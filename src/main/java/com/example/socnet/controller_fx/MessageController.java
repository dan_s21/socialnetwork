package com.example.socnet.controller_fx;

import com.example.socnet.domain.model.Message;
import com.example.socnet.domain.util.Constants;
import com.example.socnet.domain.util.Observer;
import com.example.socnet.resources.Resources;
import com.example.socnet.service.CurrentUser;
import com.example.socnet.service.MessageService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class MessageController implements Initializable, Observer {

    @FXML public ListView<Message> listViewMessages;
    @FXML public TextField textFieldMessage;
    @FXML public Button btnSendMessage;
    @FXML public Button btnGoBack;

    private final ObservableList<Message> messages;

    private final MessageService messageService;

    public MessageController() throws SQLException {
        this.messages = FXCollections.observableArrayList();
        this.messageService = Resources.getInstance()
                .getChatSuperService()
                .messageService();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.listViewMessages.setItems(this.messages);
        try {
            this.messages.addAll(this.messageService.getAll());
            this.messageService.addObserver(this);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update() {
        try {
            final ArrayList<Message> ms = messageService.getAll();
            this.messages
                    .add(ms.get(ms.size() - 1));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void onSendMessage() {
        final String messageBody = this.textFieldMessage.getText();
        if (!messageBody.isBlank()) {
            try {
                this.messageService.add(
                        CurrentUser.getInstance().getUser().getId(),
                        CurrentUser.getInstance().getSelectedChatId(),
                        messageBody
                );
                this.textFieldMessage.clear();
            } catch (final Exception e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    public void goBack(ActionEvent event) {
        NavController.navigate(Constants.UI.Scene.MAIN_FEED, event);
    }
}
