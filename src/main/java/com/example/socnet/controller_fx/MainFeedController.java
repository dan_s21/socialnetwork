package com.example.socnet.controller_fx;

import com.example.socnet.domain.model.Chat;
import com.example.socnet.domain.util.Constants;
import com.example.socnet.resources.Resources;
import com.example.socnet.service.ChatService;
import com.example.socnet.service.CurrentUser;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class MainFeedController implements Initializable {

    @FXML public Button btnShowFriendRequests;
    @FXML public Button btnSearch;
    @FXML public ListView<Chat> listViewChats;

    private final ObservableList<Chat> chats;
    private final ChatService chatService;

    public MainFeedController() throws SQLException {
        this.chats = FXCollections.observableArrayList();
        this.chatService = Resources.getInstance()
                .getChatSuperService()
                .chatService();
    }

    @FXML
    public void showFriendRequests(ActionEvent event) {
        NavController.navigate(Constants.UI.Scene.REQUESTS, event);
    }

    @FXML
    public void searchUsers(ActionEvent event) {
        NavController.navigate(Constants.UI.Scene.SEARCH, event);
    }


    @Override
    public void initialize(final URL location,
                           final ResourceBundle resources) {
        try {
            this.listViewChats.setItems(this.chats);
            this.chats.addAll(chatService.getAll());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        this.listViewChats.setOnMouseClicked(event -> {
            final Chat chat = listViewChats
                    .getSelectionModel()
                    .getSelectedItems()
                    .get(0);
            if (chat != null) {
                CurrentUser.getInstance()
                        .setSelectedChatId(chat.getId());
                NavController.navigate(Constants.UI.Scene.MESSAGES, event);
            }
        });
    }
}
