package com.example.socnet.domain.util;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

public class HashPasswordUtil {

    public static @NotNull String hash(@NotNull final String password) {
        char[] chars = password.toCharArray();
        for (int i = 0; i < chars.length; ++i) {
            chars[i] += Constants.Other.PASSWORD_ENCRYPT_KEY;
        }
        return Arrays.toString(chars);
    }
}
