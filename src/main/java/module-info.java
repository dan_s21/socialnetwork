module com.example.socnet {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires org.kordamp.bootstrapfx.core;
    requires javafx.graphics;
    requires org.jetbrains.annotations;
    requires java.sql;

    opens com.example.socnet to javafx.fxml;
    exports com.example.socnet;
    exports com.example.socnet.controller_fx;
    opens com.example.socnet.controller_fx;
    exports com.example.socnet.domain.model;
}